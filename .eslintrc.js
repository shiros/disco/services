/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : .eslintrc.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

module.exports = {
    // --------------------------------
    // General Settings

    env: {
        browser: true,
        es6: true,
        node: true
    },

    // Extends other configs
    'extends': [
        'eslint:recommended'
    ],

    // Rules
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
    },

    // --------------------------------
    // Parser Settings

    parser: "@babel/eslint-parser",

    // Options
    parserOptions: {
        sourceType: 'module',
        allowImportExportEverywhere: true
    }
};
