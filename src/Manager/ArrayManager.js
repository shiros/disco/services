/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ArrayManager.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements - Managers
import ObjectManager from "./ObjectManager";
import TypeManager from "./TypeManager";
import ValueManager from "./ValueManager";

/**
 * Manager : Array.
 */
export default class ArrayManager {
    // --------------------------------
    // Has

    /**
     * Check if a value exists in array.
     *
     * To check if an element exists, pass a callback as second parameter.
     * To check if a function exists, set 'forceAsValue' to 'true'.
     *
     * @param array
     * @param valueOrCallback
     * @param forceAsValue
     *
     * @return Boolean
     */
    static has(array, valueOrCallback, forceAsValue = false) {
        // Check
        if (!TypeManager.isArray(array)) return false;

        // Process
        return TypeManager.isFunction(valueOrCallback) && !forceAsValue
            ? array.findIndex(valueOrCallback) !== -1
            : array.includes(valueOrCallback);
    }

    /**
     * Check if index exists.
     *
     * To allow index value as nullable, set 'nullable' to 'true'.
     *
     * @param array
     * @param index
     * @param nullable
     *
     * @return Boolean
     */
    static hasIndex(array, index, nullable = false) {
        // Process
        return TypeManager.isArray(array)
            && !TypeManager.isUndefined(array[index])
            && (nullable ? true : array[index] !== null);
    }

    // --------------------------------
    // Getters

    /**
     * Get an value in array.
     *
     * If the value / element doesn't exists, you can get a default value by set 'defaultValue' to what you want.
     * To get one value / element, set 'one' to 'true'.
     *
     * @param array
     * @param callback
     * @param defaultValue
     * @param one
     *
     * @return {Array|*|null}
     */
    static get(array, callback, defaultValue = null, one = false) {
        // ----------------
        // Vars

        let results = this.filter(array, callback);

        // ----------------
        // Process

        // Case : Is Empty
        if (TypeManager.isEmpty(results)) return defaultValue;

        // Case : One
        if (ValueManager.getBoolean(one)) return this.getFirst(results, defaultValue);

        // Case : Default
        return results;
    }

    /**
     * Get an first value / element of array.
     *
     * If the value / element doesn't exists, you can get a default value by set 'defaultValue' to what you want.
     *
     * @param array
     * @param defaultValue
     * @return {*|null}
     */
    static getFirst(array, defaultValue = null) {
        // ----------------
        // Check

        if (!TypeManager.isArray(array)) return defaultValue;

        // ----------------
        // Vars

        let index = 0;

        // ----------------
        // Process

        // Case : First index exist
        if (this.hasIndex(array, index)) return array[index];

        // Case : Default
        return defaultValue;
    }

    /**
     * Get an last value / element of array.
     *
     * If the value / element doesn't exists, you can get a default value by set 'defaultValue' to what you want.
     *
     * @param array
     * @param defaultValue
     * @return {*|null}
     */
    static getLast(array, defaultValue = null) {
        // ----------------
        // Check

        if (!TypeManager.isArray(array)) return defaultValue;

        // ----------------
        // Vars

        let index = array.length - 1;

        // ----------------
        // Process

        // Case : Last index exist
        if (this.hasIndex(array, index)) return array[index];

        // Case : Default
        return defaultValue;
    }

    // --------------------------------
    // Equals

    /**
     * Check if arrays are equals.
     *
     * @param arrays
     *
     * @return Boolean
     */
    static equals(...arrays) {
        // ----------------
        // Vars

        let first = arrays.shift();

        // ----------------
        // Check

        if (!TypeManager.isArray(first)) return false;
        if (TypeManager.isEmpty(arrays)) return true;

        // ----------------
        // Process

        return arrays.every((array) => {
            // Vars
            let equals = true;

            // Check
            if (!TypeManager.isArray(array)) return false;
            if (first.length !== array.length) return false;

            // Process
            for (let index in first) {
                // Get data
                let firstValue = first[index];
                let arrayValue = array[index];

                // Process
                equals = ValueManager.equals(firstValue, arrayValue);

                // Check equals
                if (!equals) break;
            }

            return equals;
        });
    }

    // --------------------------------
    // Filter

    /**
     * Filter array.
     *
     * @param array
     * @param callback
     *
     * @return Array
     */
    static filter(array, callback) {
        // Check
        if (!TypeManager.isArray(array)) return [];
        if (!TypeManager.isFunction(callback)) return array;

        // Filter
        return array.filter(callback);
    }

    // --------------------------------
    // Merge

    /**
     * Merge arrays.
     *
     * @param arrays
     *
     * @return Array
     */
    static merge(...arrays) {
        return arrays.reduce((current, element) => {
            // Check
            if (!TypeManager.isArray(element)) return current;

            // Process
            return current.concat(element);
        }, []);
    }

    /**
     * Merge arrays recursively.
     *
     * @param arrays
     *
     * @return Array
     */
    static mergeRecursive(...arrays) {
        return arrays.reduce((current, element) => {
            // Check
            if (!TypeManager.isArray(element)) return current;

            // Process
            element.forEach((value, index) => {
                // Get value
                let currentValue = ObjectManager.get(current, index, true);

                // Case : Both are array
                if (TypeManager.isArray(currentValue) && TypeManager.isArray(value)) {
                    current[index] = this.mergeRecursive(currentValue, value);
                    return;
                }

                // Case : Both are object
                if (TypeManager.isObject(currentValue) && TypeManager.isObject(value)) {
                    current[index] = ObjectManager.mergeRecursive(currentValue, value);
                    return;
                }

                // Case : Default
                current.push(value);
            });

            return current;
        }, []);
    }

    // --------------------------------
    // Remove

    /**
     * Remove a value / element in array.
     *
     * To remove an element, pass a callback as second parameter.
     * To remove a function, set 'forceAsValue' to 'true'.
     *
     * @param array
     * @param valueOrCallback
     * @param forceAsValue
     *
     * @return Array
     */
    static remove(array, valueOrCallback, forceAsValue = false) {
        // Check
        if (!TypeManager.isArray(array)) return [];

        // Process
        return TypeManager.isFunction(valueOrCallback) && !forceAsValue
            ? this.filter(array, valueOrCallback)
            : this.filter(array, (item) => !ValueManager.equals(item, valueOrCallback));
    }

    /**
     * Remove a index in array.
     *
     * @param array
     * @param index
     *
     * @return Array
     */
    static removeIndex(array, index) {
        // Check
        if (!TypeManager.isArray(array)) return [];
        if (!this.hasIndex(array, index, true)) return array;

        // Process
        array.splice(index, 1);
        return array;
    }

    // --------------------------------
    // Clone

    /**
     * Clone array.
     *
     * @param array
     *
     * @returns Array
     */
    static clone(array) {
        // Check
        if (!TypeManager.isArray(array)) return [];

        // Process
        return array.slice(0);
    }

    // --------------------------------
    // Clear

    /**
     * Clear array.
     *
     * @param array
     *
     * @return Array
     */
    static clear(array) {
        // Check
        if (!TypeManager.isArray(array)) return [];

        // Process
        array.length = 0;
        return array;
    }

    /**
     * Clear duplicates.
     *
     * Params :
     *  - array         : The array on which to apply the method
     *  - keyOrCallback : The key or the callback system. (Use only if each item is an object). If it's a key, it
     * must be a string or number. If it's a callback, it must be a function. By default, apply classic js array
     * filter.
     *
     * Callback :
     *  - map   : This is the map / object in which the final elements must be placed.
     *  - item  : This is an item of the array.
     *  - index : This is the item index.
     *
     * @param array
     * @param keyOrCallback
     *
     * @return Array
     */
    static clearDuplicates(array = [], keyOrCallback = null) {
        // --------------------------------
        // Check

        // Array
        if (!TypeManager.isArray(array)) return [];

        // --------------------------------
        // Case : By Key or Callback

        if (!TypeManager.isNull(keyOrCallback)) {
            // ----------------
            // Vars

            let keyCondition = TypeManager.isString(keyOrCallback) || TypeManager.isNumber(keyOrCallback);
            let callbackCondition = TypeManager.isFunction(keyOrCallback);

            let map = new Map();
            let key = keyCondition ? keyOrCallback : null;
            let callback = callbackCondition ? keyOrCallback : null;

            // ----------------
            // Prepare

            // Callback
            if (!TypeManager.isNull(key)) {
                callback = (map, item) => {
                    // Get data
                    let id = ObjectManager.get(item, key);

                    // Check : Item is not an object
                    if (!TypeManager.isObject(item)) return;

                    // Check : Identifier is null
                    if (TypeManager.isNull(id)) return;

                    // Check : Identifier exist in unique set
                    if (map.has(id)) return;

                    // Set id & item
                    map.set(id, item);
                };
            }

            // ----------------
            // Process

            // Check callback
            if (!TypeManager.isFunction(callback)) return array;

            // Clear duplicates
            array.forEach((item, index) => callback(map, item, index));

            // Return new array
            return Array.from(map.values());
        }

        // --------------------------------
        // Case : Default

        // Return new array
        return Array.from(new Set(array));
    }
}
