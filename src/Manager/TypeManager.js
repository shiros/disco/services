/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : TypeManager.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements - Managers
import ObjectManager from "./ObjectManager";

/**
 * Manager : Type.
 */
export default class TypeManager {
    // --------------------------------
    // Primary

    /**
     * Check if the value is undefined.
     *
     * @param value
     * @returns Boolean
     */
    static isUndefined(value) {
        return typeof value === 'undefined';
    }

    /**
     * Check if the value is null.
     *
     * @param value
     * @returns Boolean
     */
    static isNull(value) {
        return value === null;
    }

    /**
     * Check if the value is empty.
     *
     * @param value
     * @returns Boolean
     */
    static isEmpty(value) {
        if (this.isNull(value)) return true;

        if (this.isObject(value)) {
            for (let key in value) {
                if (Object.prototype.hasOwnProperty.call(value, key)) return false;
            }
            return true;
        }
        return (value === '' || value.length <= 0);
    }

    /**
     * Check if the value is a string.
     *
     * @param value
     * @returns Boolean
     */
    static isString(value) {
        return typeof value === 'string' || value instanceof String;
    }

    /**
     * Check if the value is a number.
     *
     * @param value
     * @returns Boolean
     */
    static isNumber(value) {
        return typeof value === 'number' && isFinite(value);
    }

    /**
     * Check if the value is a not number.
     *
     * @param value
     * @returns Boolean
     */
    static isNaN(value) {
        return isNaN(value);
    }

    /**
     * Check if the value is a boolean.
     *
     * @param value
     * @returns Boolean
     */
    static isBoolean(value) {
        return typeof value === 'boolean';
    }

    /**
     * Check if the value is a date.
     *
     * @param value
     * @returns Boolean
     */
    static isDate(value) {
        return value instanceof Date;
    }

    /**
     * Check if the value is a hexadecimal color.
     *
     * @param value
     * @returns Boolean
     */
    static isHexColor(value) {
        let regex = /^#(([a-f\d])([a-f\d])([a-f\d])){1,2}$/i;
        return !this.isNull(value)
            && this.isString(value)
            && regex.exec(value) !== null
            && value.length <= 7;
    }

    // --------------------------------
    // Complex

    /**
     * Check if the value is an array.
     *
     * @param value
     * @returns Boolean
     */
    static isArray(value) {
        // return value && typeof value === 'object' && value.constructor === Array;
        return !this.isUndefined(value) && !this.isNull(value) && typeof value === 'object' && value.constructor === Array;
    }

    /**
     * Check if the value is an object.
     *
     * @param value
     * @returns Boolean
     */
    static isObject(value) {
        // return value && typeof value === 'object' && value.constructor !== Array;
        return !this.isUndefined(value) && !this.isNull(value) && typeof value === 'object' && value.constructor !== Array;
    }

    /**
     * Check if the value is a function.
     *
     * @param value
     * @returns Boolean
     */
    static isFunction(value) {
        return typeof value === 'function';
    }

    /**
     * Check if the value is a JS Node.
     *
     * @param value
     * @returns Boolean
     */
    static isNode(value) {
        return (typeof Node === "object")
            ? value instanceof Node
            : value && typeof value === "object" && typeof value.nodeType === "number" && typeof value.nodeName === "string";
    }

    /**
     * Check if the value is a DOM Element.
     *
     * @param value
     * @returns Boolean
     */
    static isElement(value) {
        return (typeof HTMLElement === "object")
            ? value instanceof HTMLElement
            : value && typeof value === "object" && typeof value.nodeType === "number" && typeof value.nodeName === "string";
    }

    /**
     * Check if the value is a File.
     *
     * @param value
     * @returns Boolean
     */
    static isFile(value) {
        return ObjectManager.is(value, File);
    }
}
