/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ValueManager.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements - Managers
import ArrayManager from "./ArrayManager";
import ObjectManager from "./ObjectManager";
import TypeManager from "./TypeManager";

/**
 * Manager : Value.
 */
export default class ValueManager {
    // --------------------------------
    // Getter

    /**
     * Get string result for a value.
     * - String (Depends of variable content)
     * - Default ''
     *
     * @param value
     * @param defaultValue
     *
     * @returns String
     */
    static getString(value, defaultValue = '') {
        // Case : Is Boolean
        if (TypeManager.isBoolean(value)) return value.toString();

        // Case : Is String
        if (TypeManager.isString(value)) return value.toString();

        // Case : Is Number
        if (TypeManager.isNumber(value)) return value.toString();

        // Case : Is File
        if (TypeManager.isFile(value)) return value.name;

        // Case : Default
        return defaultValue;
    }

    /**
     * Get number result for a value.
     * - Number (Depends of variable content)
     * - Default 0
     *
     * @param value
     * @param defaultValue
     *
     * @returns Number
     */
    static getNumber(value, defaultValue = 0) {
        // Check
        if (TypeManager.isNull(value) || TypeManager.isEmpty(value)) return defaultValue;

        // Process
        if (!TypeManager.isNumber(value)) value = Number(value);
        if (!TypeManager.isNumber(value)) return defaultValue;

        return value;
    }

    /**
     * Get boolean result for a value.
     * - True or False (Depends of variable content)
     * - Default False
     *
     * @param value
     * @param defaultValue
     *
     * @return Boolean
     */
    static getBoolean(value, defaultValue = false) {
        // Case : Is Boolean
        if (TypeManager.isBoolean(value)) return value;

        // Case : Is String
        if (TypeManager.isString(value)) return (value === 'true' || value === '1');

        // Case : Is Number
        if (TypeManager.isNumber(value)) return value === 1;

        // Case : Is Array
        if (TypeManager.isArray(value)) return !TypeManager.isEmpty(value);

        // Case : Default
        return defaultValue;
    }

    /**
     * Get hex color result for a value.
     * - Hex String Color (Depends of variable content)
     * - Default '#FFFFFF'
     *
     * @param value
     * @param defaultValue
     *
     * @returns String
     */
    static getHexColor(value, defaultValue = '#FFFFFF') {
        // ----------------
        // Check

        // Is HEX Color
        if (TypeManager.isHexColor(value)) return value.toString();

        // ----------------
        // Prepare

        value = this.getRGBColor(value, null);

        // ----------------
        // Process

        // Check
        if (TypeManager.isNull(value)) return defaultValue;

        let r = this.transformToHex(value.r);
        let g = this.transformToHex(value.g);
        let b = this.transformToHex(value.b);

        return `#${r}${g}${b}`;
    }

    /**
     * Get rgb color result for a value.
     * - Object {r: number, b: number, g: number} (Depends of variable content)
     * - Default {r: 255, g: 255, b: 255}
     *
     * @param value
     * @param defaultValue
     *
     * @returns {{r: number, b: number, g: number}}
     */
    static getRGBColor(value, defaultValue = {r: 255, g: 255, b: 255}) {
        // ----------------
        // Prepare

        // Case : Is HEX Color
        if (TypeManager.isHexColor(value)) {
            // Remove '#'
            value = value.toString().replace('#', '');

            // Get the number of characters per item
            let split = Math.trunc(value.length / 3);

            // Slip in X Parts
            value = this.match(value, {pattern: `.{1,${split}}`, flags: 'g'});

            // Prepare the split's result
            value = value.map((element) => {
                element = (split === 1) ? element.toString().repeat(2) : element;
                return parseInt(element, 16);
            })
        }

        // Case : Is String
        if (TypeManager.isString(value)) value = value.split(/ *, */g);

        // Case : Is Object
        if (ObjectManager.hasKey(value, 'r')
            && ObjectManager.hasKey(value, 'g')
            && ObjectManager.hasKey(value, 'b')
        ) {
            value = [value.r, value.g, value.b];
        }

        // Check
        if (!TypeManager.isArray(value) || value.length !== 3) return defaultValue;

        // Value
        let r = this.getNumber(value[0]);
        let g = this.getNumber(value[1]);
        let b = this.getNumber(value[2]);

        // ----------------
        // Process

        // Check
        if (r < 0 || r > 255
            || g < 0 || g > 255
            || b < 0 || b > 255
        ) return defaultValue;

        return {r: r, g: g, b: b};
    }

    /**
     * Get array result for a value.
     *
     * @param value
     * @param defaultValue
     *
     * @return Array
     */
    static getArray(value, defaultValue = []) {
        // Case : Is Array
        if (TypeManager.isArray(value)) return value;

        // Case : Is Object
        if (TypeManager.isObject(value)) return ObjectManager.getValues(value);

        // Case : Default
        return defaultValue;
    }

    /**
     * Get object result for a value.
     *
     * @param value
     * @param defaultValue
     *
     * @return Object
     */
    static getObject(value, defaultValue = {}) {
        // Case : Is Object
        if (TypeManager.isObject(value)) return value;

        // Case : Default
        return defaultValue;
    }

    // --------------------------------
    // Equals

    /**
     * Check if values are equals.
     *
     * @param values
     *
     * @return Boolean
     */
    static equals(...values) {
        // Vars
        let first = values.shift();

        // Check
        if (TypeManager.isEmpty(values)) return true;

        // Process
        return values.every((value) => {
            // Case : Both are array
            if (TypeManager.isArray(first) && TypeManager.isArray(value)) return ArrayManager.equals(first, value);

            // Case : Both are object
            if (TypeManager.isObject(first) && TypeManager.isObject(value)) return ObjectManager.equals(first, value);

            // Case : Default
            return first === value;
        });
    }

    // --------------------------------
    // Transform

    /**
     * Make the first letter of string to uppercase.
     *
     * @param value
     *
     * @return String
     */
    static ucFirst(value) {
        // Prepare
        value = this.getString(value);

        // Process
        return value.charAt(0).toUpperCase() + value.slice(1);
    }

    /**
     * Transform value to hex value.
     *
     * @param value
     * @param defaultValue
     *
     * @return String|null
     */
    static transformToHex(value, defaultValue = null) {
        // Vars
        let hex = this.getNumber(value, null);

        // Check
        if (TypeManager.isNull(hex)) return defaultValue;

        // Process
        hex = hex.toString(16);
        return (hex.length < 2) ? `0${hex}` : hex;
    }

    // --------------------------------
    // Utils

    /**
     * Check if a value match with regex.
     *
     * @param value
     * @param regexp
     *
     * @return Boolean
     */
    static match(value, regexp) {
        // ----------------
        // Prepare

        value = this.getString(value);

        // ----------------
        // Process

        // Case : Regexp is an object RegExp (Built-in)
        if (ObjectManager.is(regexp, RegExp)) {
            return regexp.exec(value);
        }

        // Case : Regexp is an object
        if (TypeManager.isObject(regexp)) {
            // Get data
            let pattern = ObjectManager.get(regexp, 'pattern');
            let flags = ObjectManager.get(regexp, 'flags');

            // Prepare
            pattern = this.getString(pattern, null);
            flags = this.getString(flags, null);

            // Check
            if (TypeManager.isNull(pattern)) return false;

            // Process
            regexp = TypeManager.isNull(flags)
                ? new RegExp(pattern)
                : new RegExp(pattern, flags);

            return this.getBoolean(regexp.exec(value));
        }

        // Case : Default
        return value.match(regexp) !== null;
    }

    /**
     * Get a random between 2 numbers.
     *
     * @param min
     * @param max
     * @return Number
     */
    static random(min, max) {
        // Prepare
        min = this.getNumber(min, 0);
        max = this.getNumber(max, 1);

        // Process
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     * Truncate a string.
     *
     * @param value
     * @param size
     *
     * @return String
     */
    static truncate(value, size) {
        // Prepare
        value = this.getString(value);
        size = this.getNumber(size);

        // Process
        return (value.length > size) ? value.substring(0, size) : value;
    }

    /**
     * Split value by a separator.
     *
     * @param value
     * @param separator
     *
     * @return Array
     */
    static split(value, separator) {
        // Prepare
        value = this.getString(value);
        separator = this.getString(separator, null);

        // Check
        if (TypeManager.isNull(separator)) return value;

        // Process
        return value.split(separator);
    }

    /**
     * Join value by a separator.
     *
     * @param value
     * @param separator
     *
     * @return String
     */
    static join(value, separator) {
        // Prepare
        value = this.getArray(value);
        separator = this.getString(separator, null);

        // Check
        if (TypeManager.isNull(separator)) return value;

        // Process
        return value.join(separator);
    }
}
