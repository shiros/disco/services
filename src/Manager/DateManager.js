/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DateManager.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements - Managers
import TypeManager from "./TypeManager";
import ValueManager from "./ValueManager";

/**
 * Manager : Date.
 */
export default class DateManager {
    // --------------------------------
    // Convert

    /**
     * Convert : Value to a date object.
     *
     * If the value can't convert, you can get a default value by set 'defaultValue' to what you want.
     * To convert using millis, set 'useMillis' to 'true'.
     *
     * @param value
     * @param defaultValue
     * @param useMillis
     *
     * @return Date|null
     */
    static convert(value, defaultValue = null, useMillis = false) {
        // Vars
        value = ValueManager.getNumber(value, value);
        useMillis = ValueManager.getBoolean(useMillis);

        // Prepare
        if (!useMillis && TypeManager.isNumber(value)) value *= 1000;

        // Date
        if (!TypeManager.isDate(value)) value = new Date(value);
        if (TypeManager.isNaN(value)) return defaultValue;

        // Process
        return value;
    }

    /**
     * Convert : Value in timestamp. (UTC)
     *
     * If the value can't convert, you can get a default value by set 'defaultValue' to what you want.
     * To convert using millis, set 'useMillis' to 'true'.
     *
     * @param value
     * @param defaultValue
     * @param useMillis
     *
     * @return Number|null
     */
    static convertToTimestamp(value, defaultValue = null, useMillis = false) {
        // Vars
        useMillis = ValueManager.getBoolean(useMillis);
        let date = this.convert(value, defaultValue, useMillis);

        // Check
        if (!TypeManager.isDate(date)) return defaultValue;

        // Process
        return useMillis ? date.getTime() : Math.floor(date.getTime() / 1000);
    }

    // --------------------------------
    // Display

    /**
     * Display : Value as date.
     *
     * @param value
     * @param locale
     * @param options
     *
     * @return String|null
     */
    static displayDate(value, locale = 'fr', options = {}) {
        // Vars
        let date = this.convert(value);

        // Check
        if (TypeManager.isNull(date)) return null;
        if (TypeManager.isNull(locale)) locale = 'fr';

        // Process
        return date.toLocaleString(locale, options);
    }
}
