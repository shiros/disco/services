/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ObjectManager.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements - Managers
import ArrayManager from "./ArrayManager";
import TypeManager from "./TypeManager";
import ValueManager from "./ValueManager";

/**
 * Manager : Object.
 */
export default class ObjectManager {
    // --------------------------------
    // Check Instance

    /**
     * Check if object is an instance of {objectClass}
     *
     * @param object
     * @param objectClass
     *
     * @return Boolean
     */
    static is(object, objectClass) {
        // Vars
        let is = false;

        // Process : Instance
        try {
            return object instanceof objectClass;
        } catch (e) {
            is = false;
        }

        // Process : Constructor
        try {
            return object instanceof objectClass.constructor;
        } catch (e) {
            is = false;
        }

        return is;
    }

    // --------------------------------
    // Has

    /**
     * Check if object have value(s).
     *
     * To check if an element exists, pass a callback as second parameter.
     * To check if a function exists, set 'forceAsValue' to 'true'.
     *
     * @param object
     * @param valueOrCallback
     * @param forceAsValue
     *
     * @return Boolean
     */
    static has(object, valueOrCallback, forceAsValue = false) {
        // Vars
        let values = this.getValues(object);

        // Check
        if (TypeManager.isEmpty(values)) return false;

        // Process
        return ArrayManager.has(values, valueOrCallback, forceAsValue);
    }

    /**
     * Check if object have key.
     *
     * @param object
     * @param key
     * @param nullable
     *
     * @return Boolean
     */
    static hasKey(object, key, nullable = false) {
        // Process
        return TypeManager.isObject(object)
            && Object.prototype.hasOwnProperty.call(object, key)
            && (nullable ? true : !TypeManager.isNull(object[key]));
    }

    /**
     * Check if object have method.
     *
     * @param object
     * @param method
     *
     * @return {boolean}
     */
    static hasMethod(object, method) {
        // Process
        return TypeManager.isObject(object)
            && TypeManager.isFunction(object[method]);
    }

    // --------------------------------
    // Getters

    /**
     * Get key in object.
     *
     * @param object
     * @param key
     * @param nullable
     * @param defaultValue
     *
     * @return {*}
     */
    static get(object, key, nullable = false, defaultValue = null) {
        // Check
        if (!this.hasKey(object, key, nullable)) return defaultValue;

        // Process
        return object[key];
    }

    /**
     * Get object keys.
     *
     * @param object
     *
     * @return Array
     */
    static getKeys(object) {
        // Check
        if (!TypeManager.isObject(object)) return [];

        // Process
        return Object.keys(object);
    }

    /**
     * Get object values.
     *
     * @param object
     *
     * @return Array
     */
    static getValues(object) {
        // Check
        if (!TypeManager.isObject(object)) return [];

        // Process
        return Object.values(object);
    }

    // --------------------------------
    // Equals

    /**
     * Check if objects are equals.
     *
     * @param objects
     *
     * @return Boolean
     */
    static equals(...objects) {
        // Vars
        let first = objects.shift();
        let firstKeys = this.getKeys(first);

        // Check
        if (!TypeManager.isObject(first)) return false;
        if (TypeManager.isEmpty(objects)) return true;

        // Process
        return objects.every((object) => {
            // Vars
            let equals = true;
            let objectKeys = this.getKeys(object);

            // Check
            if (!TypeManager.isObject(object)) return false;
            if (firstKeys.length !== objectKeys.length) return false;

            // Process
            for (let key of firstKeys) {
                // Get data
                let firstValue = first[key];
                let objectValue = object[key];

                // Process
                equals = ValueManager.equals(firstValue, objectValue);

                // Check equals
                if (!equals) break;
            }

            return equals;
        });
    }

    // --------------------------------
    // Merge

    /**
     * Merge objects.
     *
     * @param objects
     *
     * @return Object
     */
    static merge(...objects) {
        return objects.reduce((current, element) => {
            // Vars
            let keys = this.getKeys(element);

            // Process
            keys.forEach(key => current[key] = this.get(element, key, true));

            return current;
        }, {});
    }

    /**
     * Merge objects recursively.
     *
     * @param objects
     *
     * @return Object
     */
    static mergeRecursive(...objects) {
        return objects.reduce((current, element) => {
            // Vars
            let keys = this.getKeys(element);

            // Process
            keys.forEach(key => {
                // Get values
                let currentValue = ObjectManager.get(current, key, true);
                let elementValue = ObjectManager.get(element, key, true);

                // Case : Both are array
                if (TypeManager.isArray(currentValue) && TypeManager.isArray(elementValue)) {
                    current[key] = ArrayManager.mergeRecursive(currentValue, elementValue);
                    return;
                }

                // Case : Both are object
                if (TypeManager.isObject(currentValue) && TypeManager.isObject(elementValue)) {
                    current[key] = this.mergeRecursive(currentValue, elementValue);
                    return;
                }

                // Case : Default
                current[key] = elementValue;
            });

            return current;
        }, {});
    }


    // --------------------------------
    // Remove

    /**
     * Remove a value / element in object.
     *
     * To remove an element, pass a callback as second parameter.
     * To remove a function, set 'forceAsValue' to 'true'.
     *
     * @param object
     * @param valueOrCallback
     * @param forceAsValue
     *
     * @return Object
     */
    static remove(object, valueOrCallback, forceAsValue = false) {
        // Check
        if (!TypeManager.isObject(object)) return {};

        // Process
        for (let key in object) {
            // Vars
            let value = object[key];

            // Case : Has Callback
            if (TypeManager.isFunction(valueOrCallback) && !forceAsValue && valueOrCallback(value, key, object)) {
                delete object[key];
                continue;
            }

            // Case : Default
            if (ValueManager.equals(value, valueOrCallback)) {
                delete object[key];
            }
        }

        return object;
    }

    /**
     * Remove a key in object.
     *
     * @param object
     * @param key
     *
     * @return Object
     */
    static removeKey(object, key) {
        // Check
        if (!TypeManager.isObject(object)) return {};
        if (!ObjectManager.hasKey(object, key, true)) return object;

        // Process
        delete object[key];
        return object;
    }

    // --------------------------------
    // Clone

    /**
     * Clone an Object.
     *
     * @param object
     *
     * @return Object
     */
    static clone(object) {
        // Check
        if (!TypeManager.isObject(object)) return {};

        // Process
        return Object.assign({}, object);
    }

    // --------------------------------
    // Clear

    /**
     * Clear object.
     *
     * @param object
     *
     * @return Object
     */
    static clear(object) {
        // Check
        if (!TypeManager.isObject(object)) return {};

        // Process
        object = {};
        return object;
    }
}
