/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileManager.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed library
import $ from 'jquery';
import Filesize from 'filesize';

// Require needed elements - Constants
import FileConstant from "../Constant/FileConstant";
import RegexpConstant from "../Constant/RegexpConstant";

// Require needed elements - Managers
import ObjectManager from "./ObjectManager";
import TypeManager from "./TypeManager";
import ValueManager from "./ValueManager";

/**
 * Manager : File.
 */
export default class FileManager {
    // --------------------------------
    // Core Methods

    /**
     * Check if path exist.
     *
     * @param path
     *
     * @returns Promise<string>
     */
    static exists(path) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: path,
                type: 'GET',

                async: true,
                crossDomain: true,

                success: () => resolve(path),
                error: () => reject(path)
            });
        })
    }

    /**
     * Read the file corresponding to the path.
     *
     * @param path
     *
     * @returns Promise<*>
     */
    static read(path) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: path,

                async: true,
                crossDomain: true,

                success: (data) => resolve(data),
                error: (result, status, error) => reject(result, status, error)
            });
        });
    }

    // --------------------------------
    // Check Methods

    /**
     * Check the file size.
     *
     * @param file
     * @param maxSize
     */
    static checkSize(file, maxSize) {
        // Vars
        let size = this.getSize(file, null);

        // Check
        if (TypeManager.isNull(size)) return false;

        // Process
        return (size / FileConstant.SIZE_FACTOR) <= maxSize;
    }

    /**
     * Check if the value can be a image.
     *
     * @param file
     *
     * @return boolean
     */
    static isImage(file) {
        // Process
        return ValueManager.match(file, RegexpConstant.IMAGE_EXTENSION);
    }

    // --------------------------------
    // Getters

    /**
     * Get file size.
     *
     * @param file
     * @param defaultValue
     *
     * @return Number|null
     */
    static getSize(file, defaultValue = null) {
        // Check
        if (!ObjectManager.is(file, File)) return defaultValue;

        // Process
        return ValueManager.getNumber(file.size, defaultValue);
    }

    /**
     * Get file size in human readable.
     *
     * Params :
     *  - Value : Can be a file or size in bytes (Number)
     *
     * @param value
     * @param defaultValue
     * @param options
     *
     * @return String
     */
    static getSizeHumanReadable(value, defaultValue = null, options = {}) {
        // ----------------
        // Vars

        value = TypeManager.isFile(value) ? this.getSize(value) : ValueManager.getNumber(value, null);
        options = ValueManager.getObject(options);

        // ----------------
        // Check

        // Value
        if (TypeManager.isNull(value)) return defaultValue;

        // ----------------
        // Process

        // Get bytes
        let bytes = value * FileConstant.SIZE_FACTOR;

        return Filesize(bytes, options);
    }
}
