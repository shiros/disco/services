/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : IdModule.js
 * @Created_at  : 21/02/2021
 * @Update_at   : 21/02/2021
 * ----------------------------------------------------------------
 */

// Require needed libraries
import {v4 as Uuidv4} from "uuid";

// Require needed elements - Managers
import TypeManager from "../../Manager/TypeManager";
import ValueManager from "../../Manager/ValueManager";

/**
 * Module : Id.
 */
export default class IdModule {
    // --------------------------------
    // Core methods

    /**
     * Prepare id.
     *
     * @param id
     *
     * @return {Number|String}
     */
    static prepare(id) {
        // Case : Is Number
        if (TypeManager.isNumber(id)) {
            id = ValueManager.getNumber(id);
        }

        // Case : Is not a number
        if (TypeManager.isNaN(id)) {
            id = ValueManager.getString(id);
        }

        // Case : Is Null
        if (TypeManager.isNull(id)) {
            id = Uuidv4()
        }

        return id;
    }
}
