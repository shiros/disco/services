/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : ArrayManager.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed libraries
import MobileDetect from 'mobile-detect';

// Require needed elements - Constants
import RegexpConstant from "../../Constant/RegexpConstant";
import WindowConstant from "../../Constant/WindowConstant";

// Require needed elements - Managers
import ValueManager from "../../Manager/ValueManager";
import TypeManager from "../../Manager/TypeManager";

/**
 * Module : Window.
 */
export default class WindowModule {
    // --------------------------------
    // Open methods

    /**
     * Open window link.
     *
     * @param url
     * @param target
     * @param features
     * @param replace
     */
    static open(url, target = null, features = null, replace = false) {
        // Vars
        url = ValueManager.getString(url, null);
        target = ValueManager.getString(target, null);
        features = ValueManager.getString(features, null);
        replace = ValueManager.getBoolean(replace);

        // Check
        if (TypeManager.isNull(url)) throw Error('You must fill the url.');

        // Process
        window.open(url, target, features, replace);
    }

    /**
     * Open link.
     *
     * @param link
     */
    static openLink(link) {
        // Check
        if (!ValueManager.match(link, RegexpConstant.LINK)) return;

        // Process
        this.open(link, '_blank');
    }

    /**
     * Open email.
     *
     * @param email
     */
    static openEmail(email) {
        // Check
        if (!ValueManager.match(email, RegexpConstant.EMAIL)) return;

        // Vars
        let url = `mailto:${email}`;

        // Process
        this.open(url, '_self');
    }

    /**
     * Open phone.
     *
     * @param phone
     */
    static openPhone(phone) {
        // Check
        if (!ValueManager.match(phone, RegexpConstant.PHONE)) return;

        // Vars
        let url = `tel:${phone}`;

        // Process
        this.open(url, '_self');
    }

    // --------------------------------
    // Check methods

    /**
     * Check if it's a mobile navigator. (Phone & Tablet)
     *
     * @return Boolean
     */
    static isMobile() {
        // Vars
        let detector = new MobileDetect(WindowConstant.IDENTIFIER);
        let mobile = detector.mobile();

        // Process
        return !TypeManager.isNull(mobile);
    }

    /**
     * Check if it's a phone navigator.
     *
     * @return Boolean
     */
    static isPhone() {
        // Vars
        let detector = new MobileDetect(WindowConstant.IDENTIFIER);
        let phone = detector.phone();

        // Process
        return !TypeManager.isNull(phone);
    }

    /**
     * Check if it's a tablet navigator.
     *
     * @return Boolean
     */
    static isTablet() {
        // Vars
        let detector = new MobileDetect(WindowConstant.IDENTIFIER);
        let tablet = detector.tablet();

        // Process
        return !TypeManager.isNull(tablet);
    }

    // --------------------------------
    // Getters

    /**
     * Get window width.
     *
     * @return Number
     */
    static getWidth() {
        return ValueManager.getNumber(window.innerWidth);
    }

    /**
     * Get window height.
     *
     * @return Number
     */
    static getHeight() {
        return ValueManager.getNumber(window.innerHeight);
    }
}
