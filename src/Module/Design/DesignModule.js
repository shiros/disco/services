/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : DesignModule.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements - Managers
import ArrayManager from "../../Manager/ArrayManager";
import ObjectManager from "../../Manager/ObjectManager";
import ValueManager from "../../Manager/ValueManager";

/**
 * Module : Design.
 */
export default class DesignModule {
    // --------------------------------
    // Core methods

    /**
     * Process : Design.
     *
     * @param design
     *
     * @return {{class: Array, style: Object}}
     */
    static process(design = {}) {
        // ----------------
        // Vars

        let designClass = ObjectManager.get(design, 'class');
        let designStyle = ObjectManager.get(design, 'style');

        // ----------------
        // Prepare

        designClass = ValueManager.getArray(designClass);
        designStyle = ValueManager.getObject(designStyle);

        // ----------------
        // Process

        return {
            class: designClass,
            style: designStyle
        }
    }

    /**
     * Merge design.
     *
     * @param designs
     *
     * @return {{class: Array, style: Object}}
     */
    static merge(...designs) {
        return designs.reduce((current, element) => {
            // Vars
            let design = this.process(element);

            // Process
            current.class = ArrayManager.merge(current.class, design.class);
            current.style = ObjectManager.merge(current.style, design.style);

            return current;
        }, {
            class: [],
            style: {}
        });
    }
}
