/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : main.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 21/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements - Constants
import RegexpConstant from "./Constant/RegexpConstant";
import WindowConstant from "./Constant/WindowConstant";

// Require needed elements - Events
import KeyboardEvent from "./Event/KeyboardEvent";

// Require needed elements - Managers
import ArrayManager from "./Manager/ArrayManager";
import DateManager from "./Manager/DateManager";
import FileManager from "./Manager/FileManager";
import ObjectManager from "./Manager/ObjectManager";
import TypeManager from "./Manager/TypeManager";
import ValueManager from "./Manager/ValueManager";

// Require needed elements - Modules
import DesignModule from "./Module/Design/DesignModule";
import IdModule from "./Module/Id/IdModule";
import WindowModule from "./Module/Window/WindowModule";

// --------------------------------
// Export elements

export {
    // Constants
    RegexpConstant,
    WindowConstant,

    // Events
    KeyboardEvent,

    // Managers
    ArrayManager,
    DateManager,
    FileManager,
    ObjectManager,
    ValueManager,
    TypeManager,

    // Modules
    DesignModule,
    IdModule,
    WindowModule
}
