/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : KeyboardEvent.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements
import ValueManager from "../Manager/ValueManager";

/**
 * Event : Keyboard.
 */
export default class KeyboardEvent {
    // --------------------------------
    // Code

    // Keys : Alphabet
    static KEY_CODE_Z = 90;
    static KEY_CODE_F = 70;

    // Keys : Action
    static KEY_CODE_SPACE = 32;
    static KEY_CODE_RETURN = 8;
    static KEY_CODE_ENTER = 13;
    static KEY_CODE_ESCAPE = 27;

    // Keys : Arrows
    static KEY_CODE_ARROW_UP = 38;
    static KEY_CODE_ARROW_LEFT = 37;
    static KEY_CODE_ARROW_RIGHT = 39;
    static KEY_CODE_ARROW_DOWN = 40;

    // --------------------------------
    // Core methods

    /**
     * Check if the event key correspond to your key.
     *
     * @param event
     * @param key
     *
     * @return Boolean
     */
    static is(event, key) {
        // Vars
        key = ValueManager.getNumber(key);

        // Process
        return ValueManager.equals(this.getKeyCode(event), key);
    }

    /**
     * Check if the event's key code correspond to 'Z'.
     *
     * @param event
     */
    static isKeyZ(event) {
        return this.is(event, this.KEY_CODE_Z);
    }

    /**
     * Check if the event's key code correspond to 'F'.
     *
     * @param event
     */
    static isKeyF(event) {
        return this.is(event, this.KEY_CODE_F);
    }

    /**
     * Check if the event's key code correspond to 'Space'.
     *
     * @param event
     */
    static isKeySpace(event) {
        return this.is(event, this.KEY_CODE_SPACE);
    }

    /**
     * Check if the event's key code correspond to 'Return'.
     *
     * @param event
     */
    static isKeyReturn(event) {
        return this.is(event, this.KEY_CODE_RETURN);
    }

    /**
     * Check if the event's key code correspond to 'Enter'.
     *
     * @param event
     */
    static isKeyEnter(event) {
        return this.is(event, this.KEY_CODE_ENTER);
    }

    /**
     * Check if the event's key code correspond to 'ESCAPE'.
     *
     * @param event
     */
    static isKeyEscape(event) {
        return this.is(event, this.KEY_CODE_ESCAPE);
    }

    /**
     * Check if the event's key code correspond to 'Arrow Up'.
     *
     * @param event
     */
    static isKeyArrowUp(event) {
        return this.is(event, this.KEY_CODE_ARROW_UP);
    }

    /**
     * Check if the event's key code correspond to 'Arrow Left'.
     *
     * @param event
     */
    static isKeyArrowLeft(event) {
        return this.is(event, this.KEY_CODE_ARROW_LEFT);
    }

    /**
     * Check if the event's key code correspond to 'Arrow Right'.
     *
     * @param event
     */
    static isKeyArrowRight(event) {
        return this.is(event, this.KEY_CODE_ARROW_RIGHT);
    }

    /**
     * Check if the event's key code correspond to 'Arrow Down'.
     *
     * @param event
     */
    static isKeyArrowDown(event) {
        return this.is(event, this.KEY_CODE_ARROW_DOWN);
    }

    // --------------------------------
    // Getters

    /**
     * Get key code from event.
     *
     * @param event
     *
     * @return Number
     */
    static getKeyCode(event) {
        return ValueManager.getNumber(event.key);
    }
}
