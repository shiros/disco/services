/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : WindowConstant.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements - Managers
import ValueManager from "../Manager/ValueManager";

/**
 * Constant : Window.
 */
export default class WindowConstant {
    /**
     * Window identifier.
     *
     * @type String|null
     */
    static IDENTIFIER = ValueManager.getString(navigator.userAgent || navigator.vendor || window.opera, null);
}