/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : RegexpConstant.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

/**
 * Constant : Regexp.
 */
export default class RegexpConstant {
    /**
     * Regexp of web link.
     *
     * @type RegExp
     */
    static LINK = /^(((http|ftp)s?|file):\/\/|\\\\).+$/g;

    /**
     * Regexp of email.
     *
     * @type RegExp
     */
    static EMAIL = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/g;

    /**
     * Regexp of phone.
     *
     * @type RegExp
     */
    static PHONE = /^[0-9+ ()]+$/g;

    /**
     * Regexp of image extension.
     *
     * @type RegExp
     */
    static IMAGE_EXTENSION = /\.(jpg|jpeg|png|svg|bmp|gif|ico)$/gi;
}