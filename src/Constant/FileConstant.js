/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : FileConstant.js
 * @Created_at  : 13/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

/**
 * Constant : File.
 */
export default class FileConstant {
    /**
     * File size factor.
     *
     * @type Number
     */
    static SIZE_FACTOR = 1024;
}