# Disco Package - Services

This package contains all Disco's services.

## Project

Some utils commands.

```shell script
$ yarn install
$ yarn build
$ yarn watch
$ yarn test
```

## Information

You can access to the wiki [here](https://gitlab.com/shiros/disco/services/-/wikis/home).

In this project, we'll use some external libs.

### Community

- [Filesize](https://github.com/avoidwork/filesize.js), you can see the
  licence [here](https://github.com/avoidwork/filesize.js/blob/master/LICENSE).
- [Mobile Detect](https://github.com/hgoebl/mobile-detect.js), you can see the
  licence [here](https://github.com/hgoebl/mobile-detect.js/blob/master/LICENSE).
- [JQuery](https://github.com/jquery/jquery), you can see the
  licence [here](https://github.com/jquery/jquery/blob/master/LICENSE.txt).

## Installation

Follow the command below, to install this package :

### Classic

```shell
$ yarn add disco-services
```

### Specific branch

```shell
$ yarn add ssh://git@gitlab.com:shiros/disco/services.git#<branch_name>
```

### Dev

```shell
$ yarn add ssh://git@gitlab.com:shiros/disco/services.git#master
```

## Authors

- [Alexandre Caillot (Shiroe_sama)](https://gitlab.com/Shiroe_sama)
